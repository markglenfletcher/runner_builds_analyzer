# frozen_string_literal: true

require 'gitlab'

# Constants used for API calls
RUNNER_PATH = 'gitlab-org/gitlab-runner'
PER_PAGE = 100

DOCS_REGEX = /(^docs[\/-].*|.*-docs$)/

Gitlab.private_token = ENV['GITLAB_PERSONAL_TOKEN']
Gitlab.endpoint = 'https://gitlab.com/api/v4'

puts 'Pulling MRs...'

all_mrs = Gitlab.merge_requests(RUNNER_PATH,
                            {
                              PER_PAGE: PER_PAGE
                            }).auto_paginate



puts "Found #{all_mrs.length} MRs"
puts 'Filtering MRs...'
puts 'Filtering on source branch name...'

mrs_without_docs_source_branch_name = all_mrs.reject do |mr|
  mr.source_branch =~ DOCS_REGEX
end

puts "#{mrs_without_docs_source_branch_name.length} MRs with docs source branch name"
puts 'Filtering on changes...'

docs_only_changes = mrs_without_docs_source_branch_name.select do |mr|
  puts "Pulling changes for MR #{mr.id}: #{mr.web_url}"
  changes = Gitlab.merge_request_changes(RUNNER_PATH, mr.iid)

  changes_paths = changes.changes.map { |change| change['new_path'] }

  docs_changes = changes_paths.all? do |path|
    puts "Change at: #{path}"
    path.start_with?('docs/')
  end

  puts (docs_changes ? "Docs only changes detected" : "Other changes detected")

  docs_changes
end

puts "MRs with docs only changes: #{docs_only_changes.length}"

require 'byebug'
byebug

if docs_only_changes.any?
  puts docs_only_changes.map do |change|
    [change['id'], change['iid'], change['web_url']]
  end.to_a.inspect
else
  "None found!"
end


